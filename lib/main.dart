import 'package:flutter/material.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.deepPurple),
      home: Scaffold(
        appBar: AppBar(
          title: Text("NEPEN"),
        ),
        body: Container(
          color: Colors.white70,
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text("Usuário"),
                accountEmail: Text("user@nepen.org.br"),
              ),
              ListTile(
                  leading: Icon(Icons.home),
                  title: Text("Home"),
                  onTap: () {}),
              ListTile(
                  leading: Icon(Icons.bluetooth_searching),
                  title: Text("Buscar Dispositivos"),
                  onTap: () {
                    Navigator.pop(context);
                  }),
              ListTile(
                  leading: Icon(Icons.search),
                  title: Text("Obter Estado"),
                  onTap: () {
                    Navigator.pop(context);
                  }),
              ListTile(
                  leading: Icon(Icons.description),
                  title: Text("Logs de Comunicação"),
                  onTap: () {
                    Navigator.pop(context);
                  }),
              ListTile(
                  leading: Icon(Icons.settings),
                  title: Text("Configurar a NIC"),
                  onTap: () {
                    Navigator.pop(context);
                  })
            ],
          ),
        ),
      ),
    );
  }
}